<?php

include('../controlador/conexion.php');
$conexion = conectar();

//Conecta para extraer los datos  de la tabla 'expositor'
$expositor = $conexion->prepare("SELECT * FROM expositor");
$expositor->execute();
$expositor = $expositor->fetchAll();

//Conecta para extraer los nombres y comentarios de la tabla 'serie'
$serie = $conexion->prepare("SELECT * FROM serie");
$serie->execute();
$serie = $serie->fetchAll();

//Conecta para extraer los datos de la tabla 'serie_2'
$serie_2 = $conexion->prepare("SELECT * FROM serie_2");
$serie_2->execute();
$serie_2 = $serie_2->fetchAll();


if (isset($_POST['insertar'])) {

    //variables de la tabla audio bbdd //
    $nombre_audio = ($_POST['nombre_audio']);
    $carpeta_audio =  "audio/";
    $audio = trim($_FILES['audio']['name']);
    $ruta_audio = $carpeta_audio . $audio;
    $fecha_audio = ($_POST['fecha_audio']);
    $categoria = ($_POST['categoria']);
    $carpeta_pdf = "pdf/";
    $pdf = trim($_FILES['pdf']['name']);
    $ruta_pdf = $carpeta_pdf . $pdf;
    $carpeta_pptx = "pptx/";
    $pptx = trim($_FILES['pptx']['name']);
    $ruta_pptx = $carpeta_pptx . $pptx;
    $libro = ($_POST['libro']);
    $pasaje = ($_POST['pasaje']);
    $turno = ($_POST['turno']);
    $comentario_audio = ($_POST['comentario_audio']);

    //variables de las llaves foráneas
    $fk_expositor = ($_POST['nombre_expositor']);
    $fk_serie = ($_POST['nombre_serie']);
    $fk_serie_2 = ($_POST['nombre_serie_2']);

    if (move_uploaded_file($_FILES['audio']['tmp_name'], $ruta_audio)) {

        move_uploaded_file($_FILES['pdf']['tmp_name'], $ruta_pdf);
        move_uploaded_file($_FILES['pptx']['tmp_name'], $ruta_pptx);
        move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta_imagen);


        //query de inserción en la tabla audio
        $sql = $conexion->prepare("INSERT  INTO audio(nombre_audio, audio, fecha_audio, categoria, pdf, pptx, libro, pasaje, turno, comentario_audio, fk_expositor, fk_serie, fk_serie_2)
            VALUES (:nombre_audio, :audio, :fecha_audio, :categoria, :pdf, :pptx, :libro, :pasaje, :turno, :comentario_audio, :fk_expositor, :fk_serie, :fk_serie_2)");

        $sql->bindParam(':nombre_audio', $nombre_audio, PDO::PARAM_STR);
        $sql->bindParam(':audio', $audio, PDO::PARAM_LOB);
        $sql->bindParam(':fecha_audio', $fecha_audio, PDO::PARAM_STR);
        $sql->bindParam(':categoria', $categoria, PDO::PARAM_STR);
        $sql->bindParam(':pdf', $pdf, PDO::PARAM_LOB);
        $sql->bindParam(':pptx', $pptx, PDO::PARAM_LOB);
        $sql->bindParam(':libro', $libro, PDO::PARAM_STR);
        $sql->bindParam(':pasaje', $pasaje, PDO::PARAM_STR);
        $sql->bindParam(':turno', $turno, PDO::PARAM_STR);
        $sql->bindParam(':comentario_audio', $comentario_audio, PDO::PARAM_STR);
        $sql->bindParam(':fk_expositor', $fk_expositor, PDO::PARAM_STR);
        $sql->bindParam(':fk_serie', $fk_serie, PDO::PARAM_STR);
        $sql->bindParam(':fk_serie_2', $fk_serie_2, PDO::PARAM_STR);

        $sql->execute();

        header('Location: ../controlador/tabla.php');
        unset($_POST);
    } else {
        header('Location: ../index.php');
        unset($_POST);
    }
} else {

    header('Location: ../index.php');
    unset($_POST);
}
