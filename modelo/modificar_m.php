 <?php

    include('../controlador/conexion.php');
    $conexion = conectar();

    $id_audio = $_GET['id'];

    //Conecta para extraer los datos  de la tabla 'expositor'
    $expositor = $conexion->prepare("SELECT * FROM expositor");
    $expositor->execute();
    $expositor = $expositor->fetchAll();
    
    //Conecta para extraer los datos de la tabla 'serie'
    $serie = $conexion->prepare("SELECT * FROM serie");
    $serie->execute();
    $serie = $serie->fetchAll();
    
    //Conecta para extraer los datos de la tabla 'serie_2'
    $serie_2 = $conexion->prepare("SELECT * FROM serie_2");
    $serie_2->execute();
    $serie_2 = $serie_2->fetchAll();
    
    // Extrae los datos del audio seleccionado cons sus datos relacionados a otras tablas (expositor, serie y serie_2)
    $sql = $conexion->prepare("SELECT * FROM audio 
    JOIN expositor ON  audio.fk_expositor = expositor.id_expositor 
    JOIN serie ON audio.fk_serie = serie.id_serie
    JOIN serie_2 ON audio.fk_serie_2 = serie_2.id_serie_2
    WHERE id_audio = '$id_audio'");
    
    $sql->execute();
    $audio_mod = $sql->fetchAll();

    if (isset($_POST['actualizar'])) {

        //variables de la tabla audio bbdd //
        $id_audio = ($_POST['id_audio']);
        $nombre_audio = ($_POST['nombre_audio']);
        $carpeta_audio =  "../modelo/audio/";
        $audio = trim($_FILES['audio']['name']);
        $ruta_audio = $carpeta_audio . $audio;
        $fecha_audio = ($_POST['fecha_audio']);
        $categoria = ($_POST['categoria']);
        $carpeta_pdf = "../modelo/pdf/";
        $pdf = trim($_FILES['pdf']['name']);
        $ruta_pdf = $carpeta_pdf . $pdf;
        $carpeta_pptx = "../modelo/pptx/";
        $pptx = trim($_FILES['pptx']['name']);
        $ruta_pptx = $carpeta_pptx . $pptx;
        $libro = ($_POST['libro']);
        $pasaje = ($_POST['pasaje']);
        $turno = ($_POST['turno']);
        $comentario_audio = ($_POST['comentario_audio']);
        //Estas son las variables de las  llaves foráneas de las tablas relacionadas a 'audio'
        $fk_expositor = ($_POST['nombre_expositor']);
        $fk_serie = ($_POST['nombre_serie']);
        $fk_serie_2 = ($_POST['nombre_serie_2']);

      

            $sql_2 = $conexion->prepare("UPDATE  audio  SET
            nombre_audio = '$nombre_audio',
            fecha_audio = '$fecha_audio',
            categoria = '$categoria',
            pdf = '$pdf',
            pptx = '$pptx',
            libro = '$libro',
            pasaje = '$pasaje',
            turno = '$turno',
            comentario_audio = '$comentario_audio',
            fk_expositor = '$fk_expositor',
            fk_serie = '$fk_serie',
            fk_serie_2 = '$fk_serie_2'
            WHERE id_audio = '$id_audio'");
            $sql_2->execute();

            if ($sql_2) {
            header('Location: ../controlador/tabla.php');
            unset($_POST);
        } else {
            header('Location: ../index.php');
            unset($_POST);
        }
    } else {
        header('Location: ../index.php');
        unset($_POST);
}
