<?php session_start();

//nos conectamos a la base de datos
include ("../controlador/conexion.php");
$conexion = conectar();

	// Si se envían los datos entonces verifica
	if (isset($_POST['registrar'])) {
		$usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING); //filter var evita inyección de código
		$password = $_POST['password'];
		$password2 = $_POST['password2'];
		
		$errores = '';

		// Comprobamos que los tres campos no esten vacíos
		if (empty($usuario) or empty($password) or empty($password2)) {
			$errores .= '<li>Por favor completa todos los datos correctamente</li>';
		} else {

	

			// Preparamos la consulta y la enviamos a la bbdd para verificar si el nombre del usuario existe
			$sql = $conexion->prepare('SELECT * FROM usuarios WHERE usuario = :usuario LIMIT 1');
			$sql->execute(array(':usuario' => $usuario));
			$resultado = $sql->fetch(); // devuevle el registro del usuario o false (no existe en BBDD)


			if ($resultado != false) {
				$errores .= '<li>El nombre de usuario ya existe</li>';
			}

			// Encriptamos la contraseña
			$password = hash('sha512', $password);
			$password2 = hash('sha512', $password2);

			// Comprobamos que las contraseñas sean iguales
			if ($password != $password2) {
				$errores .= '<li>Las contraseñas no son iguales</li>';
			}
		}//aquí terminan las comprobaciones de errores previos

		if ($errores == '') {
			$sql = $conexion->prepare('INSERT INTO usuarios (id, usuario, pass) VALUES (null, :usuario, :pass)');
			$sql->execute(array(
				':usuario' => $usuario,
				':pass' => $password
			));
			// Al crear un  nuevo usuario se cierra la sesión del usuario administrador
			session_destroy();
			header('Location: ../controlador/login.php');
		}
	}

