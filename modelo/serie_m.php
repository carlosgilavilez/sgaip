<?php

include('../controlador/conexion.php');
$conexion = conectar();

//Conecta para extraer los nombres y comentarios de la tabla 'serie'
$serie = $conexion->prepare("SELECT * FROM serie");
$serie->execute();
$serie = $serie->fetchAll();


// Envía los datos de la nueva serie a la tabla serie de bbdd
if (isset($_POST['agregar'])) {

    $nombre_serie = ($_POST['nombre_serie']);
    $comentario_serie = ($_POST['comentario_serie']);

    //query de inserción en la tabla  serie
    $sql = $conexion->prepare("INSERT  INTO serie(nombre_serie, comentario_serie)
            VALUES (:nombre_serie, :comentario_serie)");

    $sql->bindParam(':nombre_serie', $nombre_serie, PDO::PARAM_STR);
    $sql->bindParam(':comentario_serie', $comentario_serie, PDO::PARAM_STR);
    $sql->execute();

    //query de inserción en la tabla  serie_2
    $sql_2 = $conexion->prepare("INSERT  INTO serie_2(nombre_serie_2)
    VALUES (:nombre_serie)");

    $sql_2->bindParam(':nombre_serie', $nombre_serie, PDO::PARAM_STR);
    $sql_2->execute();

    header('Location: ../controlador/serie.php');

} else {

    header('Location: ../index.php');
    unset($_POST);
}
