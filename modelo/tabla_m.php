<?php session_start();

include('../controlador/conexion.php');
$conexion = conectar();

    $pagina  = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;
    $audios_por_pagina = 7;

    //La variable '$filtros' nos devuelve los 5 audios desde la página que corresponde (si es mayor a 1)
    $filtro = ($pagina > 1) ? ($pagina * $audios_por_pagina - $audios_por_pagina) : 0;

    $audios = $conexion->prepare("SELECT SQL_CALC_FOUND_ROWS * FROM audio 
    JOIN expositor ON  audio.fk_expositor = expositor.id_expositor 
    JOIN serie ON audio.fk_serie = serie.id_serie
    JOIN serie_2 ON audio.fk_serie_2 = serie_2.id_serie_2
    LIMIT $filtro, $audios_por_pagina");
    $audios->execute();
    $audios = $audios->fetchAll();
    //print_r($audios);

if (!$audios) {

    header('Location: ../index.php');
}

    // la variab $audios nos permite conocer la cantidad de audios
    $total_audios = $conexion->query('SELECT FOUND_ROWS()  as total');
    $total_audios = $total_audios->fetch(PDO::FETCH_ASSOC)['total'];
    //echo "audios: " . $audios;

    // numero entero para la paginacion
    $numeroPaginas = ceil($total_audios / $audios_por_pagina);

    //echo "paginas: " . $numeroPaginas;

    //require 'vista/tabla_v.php';
