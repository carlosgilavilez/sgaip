<?php 
  
include ('../controlador/conexion.php');
$conexion = conectar();

//Conecta para extraer los datos  de la tabla 'expositor'
$expositor = $conexion->prepare("SELECT * FROM expositor");
$expositor->execute();
$expositor = $expositor->fetchAll();

if (isset($_POST['introducir'])){

    //variables de la tabla expositor para capturar los datos del formulario 'expositor_v.php'
    $nombre_expositor = ($_POST['nombre_expositor']);
    $carpeta_imagen = '../modelo/imagen/';
    $imagen = trim ($_FILES['imagen']['name']);
    $ruta_imagen = $carpeta_imagen . $imagen;
    $comentario_expositor = ($_POST['comentario_expositor']);
    
    move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta_imagen);

    //query de inserción en la tabla 'expositor' de la bbdd 
    $sql=$conexion->prepare("INSERT  INTO expositor(nombre_expositor, imagen, comentario_expositor) 
    VALUES (:nombre_expositor, :imagen, :comentario_expositor)
    ON DUPLICATE KEY UPDATE id_expositor = LAST_INSERT_ID(id_expositor), imagen = :imagen");
        
    $sql->bindParam(':nombre_expositor', $nombre_expositor, PDO::PARAM_STR);
    $sql->bindParam(':imagen', $imagen, PDO::PARAM_LOB);
    $sql->bindParam(':comentario_expositor', $comentario_expositor, PDO::PARAM_STR);
    $sql->execute();    
    
    header('Location: ../controlador/expositor.php');


} else{
   
        header('Location: ../index.php');
    unset($_POST);    
    
}