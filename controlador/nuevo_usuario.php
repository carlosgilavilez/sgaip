<?php session_start();

if (isset($_SESSION['usuario'])) {

    require_once '../modelo/nuevo_usuario_m.php';
    require '../vista/nuevo_usuario_v.php';
    
} else {

    header ('Location: login.php');

}
