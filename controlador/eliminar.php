<?php session_start();

include('conexion.php');
$conexion = conectar();

//capturamos el identificador del audio a eliminar
$id_audio = $_GET['id'];

//Preparamos y ejecutamos la eliminación del audio seleccionado
$sql = $conexion->prepare("DELETE  FROM audio WHERE id_audio = '$id_audio'");
$sql->execute();

if ($sql) {
    header('Location: ../controlador/tabla.php');        
}else {
    echo 'No se ha podido eliminar el audio. Díselo a Dani';
}
