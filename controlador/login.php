<?php session_start();


if (isset($_SESSION['usuario'])) {
    header('Location: ../index.php');
}

$errores = '';
//Captura y  verificación de los datos ingresados en el formulario                      
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $usuario = filter_var(strtolower($_POST['usuario']), FILTER_SANITIZE_STRING);
    $password = $_POST['password'];
    $password = hash('sha512', $password);

    //Conectamos a la base de datos
    include('conexion.php');
    $conexion = conectar();
    
    //Iniciamos la verificación de los datos ingresados en el formulario con los datos de la tabla 'usuario' en la base de datos
    $sql = $conexion->prepare('SELECT * FROM usuarios WHERE usuario = :usuario AND pass = :password');
    $sql->execute(array(':usuario' => $usuario, ':password' => $password));

    $resultado = $sql->fetch(); //el método fetch nos devuelve el resultado de la consulta sql
   
    //Si se encuentran los datos, entonces se inicia la sesión
    if ($resultado !== false) {
        $_SESSION['usuario'] = $usuario;
        header('Location: ../index.php');
    } else {
        $errores .= '<li>Datos incorrectos</li>';
    }
}

require_once '../vista/login_v.php';
