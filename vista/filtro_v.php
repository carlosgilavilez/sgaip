<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <title>Tabla</title>

    <link rel="stylesheet" type="text/css" href="css/estilos.css">
    <link rel="stylesheet" type="text/css" href="css/alertify.css">

</head>

<body>

    <h1 class="titulo">Modificar SGAIP</h1>
    <h2 style="text-align:center;"></h2>
    <a href="tabla.php">Volver</a>

    
    <div class="container-fluid">

        <div class="table-responsive-xl">
            <table class="table table-bordered table-dark  table-hover table-sm">

                <tr class="bg-primary">
                    <th class="bg-primary sticky">Nombre del audio</th>
                    <th class="bg-primary sticky">Fecha</th>
                    <th class="bg-primary sticky">url</th>
                    <th class="bg-primary sticky">Predicador/Expositor</th>
                    <th class="bg-primary sticky">Categoría</th>
                    <th class="bg-primary sticky">Pdf</th>
                    <th class="bg-primary sticky">Pptx</th>
                    <th class="bg-primary sticky">Serie</th>
                    <th class="bg-primary sticky">Texto biblíco</th>
                    <th class="bg-primary sticky">Turno</th>
                    <th class="bg-primary sticky">Comentario</th>
                    <th class="bg-primary sticky">Acción</th>

                </tr>

                <?php foreach ($audio_mod as $dato) : ?>
                    <tr>
                    <!-- <td><?php echo $dato['id_audio'] ?></td> -->
                    
                        <td><?php echo $dato['nombre_audio'] ?></td>

                        <td><?php echo $dato['fecha_audio'] ?></td>

                        <td><a href="audio/<?php echo $dato['audio']; ?>" target="_blank"><?php echo $dato['audio']; ?></a></td>

                        <td><?php echo $dato['nombre_expositor'] ?></td>

                        <td><?php echo $dato['categoria'] ?></td>

                        <td><a href="pdf/<?php echo $dato['pdf']; ?>" target="_blank"><?php echo $dato['pdf']; ?></a></td>

                        <td><a href="pptx/<?php echo $dato['pptx']; ?>"><?php echo $dato['pptx']; ?></a></td>

                        <td><?php echo $dato['nombre_serie'] ?></td>

                        <td><?php echo $dato['libro'] . " " . $dato['pasaje'] ?></td>

                        <td><?php echo $dato['turno'] ?></td>

                        <td><?php echo $dato['comentario'] ?></td>
                       <td><a href="">Eliminar</a></td>
                                             
                    </tr>
                    <!--<td style="width: 20px;"><audio controls> <source src="<?php echo 'audio/' .  $fila['audio']; ?>" type="audio/mpeg"  ></source>
                      </audio controls preload="none"></td> -->

                <?php endforeach; ?>

            </table>
        </div>

    </div>


    <div class="contenedor">
        <section class="paginacion">
            <ul>
                <!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
                <?php if ($pagina == 1) : ?>
                    <li class="disabled">&laquo;</li>
                <?php else : ?>
                    <li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
                <?php endif; ?>

                <!-- Ejecutamos un ciclo para mostrar las paginas, las cuales se crean según el num del $i -->
                <?php
                for ($i = 1; $i <= $numeroPaginas; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
                    } else {
                        echo "<li><a href='?pagina=$i'>$i</a></li>";
                    }
                }
                ?>
                <!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
                <?php if ($pagina == $numeroPaginas) : ?>
                    <li class="disabled">&raquo;</li>
                <?php else : ?>
                    <li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
                <?php endif; ?>
            </ul>
        </section>
    </div>
</body>

</html>