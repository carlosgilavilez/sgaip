<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="widlabel=device-widlabel, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../modelo/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../modelo/css/alertify.css">

    <title>Nueva serie | sgaip</title>

</head>

<body>

    <div class="container-fluid">
        <h2 class="titulo">Series</h2>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <a href="cerrar.php">Cerrar Sesion</a><br>
                <a href="panel.php">Volver al panel</a><br>
                <ul>
                    <li>Los campso con asteriscos son obligatoris</li>
                    <li>Los audios se deben insertar en formato mp3</li>
                </ul>

            </div>
            <div class="col-sm-4">
                <form action="../modelo/serie_m.php" id="form_insertar" onsubmit="return insertar_audio()" name="serie_m" method="POST" enctype="multipart/form-data" class="formulario audio">
                    <div class="form-group">
                        <label for="">Nombre</label>
                        <input type="text" class="form-control" autocomplete="on" name="nombre_serie">
                    </div>

                    <br>
                    <div class="form-group">
                        <label for="" class="">Serie</label>
                        <select id="" class="form-control" name="serie">
                            <option value=""></option>
                            <option value="Confinamiento"> Confinamiento</option>
                            <option value="El avivamiento de Josías">El avivamiento de Josías</option>
                            <option value="El Siervo de Jehová">El Siervo de Jehová</option>
                            <option value="El Siervo de Jehová">En tiempos de tormenta</option>
                            <option value="Génesis">Génesis</option>
                            <option value="Jueces">Jueces</option>
                            <option value="Las parábolas de Jesús según el evangelio de Lucas">Las parábolas de Jesús según el evangelio de Lucas</option>
                            <option value="Primera de Juan">Primera de Juan</option>
                            <option value="Primera de Samuel">Primera de Samuel</option>
                            <option value="Primera de Tesalonicenses">Primera de Tesalonicenses</option>
                            <option value="Siete amores">Siete amores</option>
                            <option value="¿Qué enseña la biblia acerca de la iglesia">¿Qué enseña la biblia acerca de la iglesia?</option>
                            <option value="El dinero">El dinero</option>
                            <option value="El evangelismo">El evangelismo</option>
                            <option value="Encuentro con Dios">Encuentro con Dios</option>
                            <option value="Masculinidad y Feminidad bíblicas">Masculinidad y Feminidad bíblicas</option>
                            <option value="Siendo luz en nuestro género">Siendo luz en nuestro género</option>
                        </select><br>
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Comentario</label>
                        <textarea class="form-control" id="" cols="10" rows="5" placeholder="" name="comentario" autocomplete="on"></textarea>
                    </div>
                    <br>
                    <br>
                    <input type="submit" class="submit-btn" style="width:100%;" value="Enviar serie" name="agregar" id="enviar">
                    <br><br><br>
                </form>
            </div><br><br><br>
            <div class="col-sm-4">
                <div class="container-fluid">

                    <div class="table-responsive-xl">
                        <table class="table table-bordered table-dark  table-hover table-sm">

                            <tr class="bg-primary">
                                <th class="bg-primary sticky">Nombre</th>
                                <th class="bg-primary sticky">Comentario</th>
                            </tr>

                            <?php include_once('../modelo/serie_m.php'); ?>
                            <?php foreach ($serie as $dato) : ?>
                                <tr>

                                    <td><?php echo $dato['nombre_serie'] ?></td>

                                    <td><?php echo $dato['comentario_serie'] ?></td>

                                </tr>

                            <?php endforeach; ?>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-migrate-3.3.1.js"></script>
    <!-- <script type="text/javascript" src="js/ajax.js"></script> -->
    <script type="text/javascript" src="../modelo/js/javascript.js"></script>
    <script type="text/javascript" src="../modelo/js/alertify.js"></script>

</body>

</html>