<?php require_once '../modelo/modificar_m.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="widlabel=device-widlabel, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../modelo/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../modelo/css/alertify.css">

    <title>Modificar | SGAIP</title>
</head>

<body>

    <div class="container-fluid">
        <h1 class="titulo">SGAIP</h1>
        <h2 style="text-align:center;">Modificar datos del audio</h2>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <a href="cerrar.php">Cerrar Sesion</a><br>
                <a href="tabla.php">Ver tabla de audios</a>

                <ul>
                    <li>Los campso con asteriscos son obligatoris</li>
                    <li>Los audios se deben insertar en formato mp3</li>
                </ul>

            </div>

            <div class="col-sm-4">
                <form action="../modelo/modificar_m.php" id="form_modificar" name="formulario_mod" method="POST" enctype="multipart/form-data" class="formulario audio">
                    <?php foreach ($audio_mod as $dato) : ?>

                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" value="<?php echo $dato['id_audio']; ?>" id="id_audio" class="form-control" autocomplete="on" name="id_audio" hidden>

                            <input type="text" value="<?php echo $dato['nombre_audio']; ?>" id="nombre_audio" class="form-control" autocomplete="on" name="nombre_audio">

                        </div>

                        <div class="form-group "><br>
                            <label for=""> Audio </label>
                            <a href="../modelo/audio/<?php echo $dato['audio']; ?>" target="_blank"><?php echo $dato['audio']; ?></a>
                            <!-- <input type="file" value="<?php echo $dato['audio']; ?>" class="form-control" id="audio_mp3" name="audio"> -->
                        </div>
                        <br>

                        <div class="form-group ">
                            <label for="">Fecha</label>
                            <input type="date" value="<?php echo $dato['fecha_audio']; ?>" class="form-control" name="fecha_audio">
                        </div>
                        <br>

                        <div class="form-group">
                            <label for="">* Categoría del audio:</label>
                            <select class="form-control" id="categoria" name="categoria">
                                <option value="<?php echo $dato['categoria']; ?>"><?php echo $dato['categoria']; ?></option>
                                <option value="Predicacion">Predicación</option>
                                <option value="tema esencial">Tema esencial</option>
                                <option value="otro">Otro</option>

                            </select>
                        </div>

                        <div class="form-row cita" style="display:none;" id="cita_bloque">
                            <div class="form-group" id="cita">
                                <label for="">Libro de la Biblia</label>
                                <select class="form-control" name="libro">
                                    <option value="<?php echo $dato['libro']; ?>"><?php echo $dato['libro']; ?></option>
                                    <option value="Génesis">Génesis</option>
                                    <option value="Éxodo">Éxodo</option>
                                    <option value="Levítico">Levítico</option>
                                    <option value="Números">Números</option>
                                    <option value="Deuteronomio">Deuteronomio</option>
                                    <option value="Josue">Josué</option>
                                    <option value="Jueces">Jueces</option>
                                    <option value="Rut">Rut</option>
                                    <option value="09">1 Samuel</option>
                                    <option value="10">2 Samuel</option>
                                    <option value="11">1 Reyes</option>
                                    <option value="12">2 Reyes</option>
                                    <option value="13">1 Crónicas</option>
                                    <option value="14">2 Crónicas</option>
                                    <option value="15">Esdras</option>
                                    <option value="Nehemías">Nehemías</option>
                                    <option value="17">Ester</option>
                                    <option value="18">Job</option>
                                    <option value="19">Salmos</option>
                                    <option value="20">Proverbios</option>
                                    <option value="21">Eclesiastés</option>
                                    <option value="22">El Cantar de los Cantares</option>
                                    <option value="23">Isaías</option>
                                    <option value="24">Jeremías</option>
                                    <option value="25">Lamentaciones</option>
                                    <option value="26">Ezequiel</option>
                                    <option value="27">Daniel</option>
                                    <option value="28">Oseas</option>
                                    <option value="29">Joel</option>
                                    <option value="30">Amós</option>
                                    <option value="31">Abdías</option>
                                    <option value="32">Jonás</option>
                                    <option value="33">Miqueas</option>
                                    <option value="34">Nahúm</option>
                                    <option value="35">Habacuc</option>
                                    <option value="36">Sofonías</option>
                                    <option value="37">Ageo</option>
                                    <option value="38">Zacarías</option>
                                    <option value="39">Malaquías</option>
                                    <option value=""></option>
                                    <option value="40">Mateo</option>
                                    <option value="41">Marcos</option>
                                    <option value="42">Lucas</option>
                                    <option value="43">Juan</option>
                                    <option value="44">Hechos</option>
                                    <option value="45">Romanos</option>
                                    <option value="46">1 Corintios</option>
                                    <option value="47">2 Corintios</option>
                                    <option value="48">Gálatas</option>
                                    <option value="49">Efesios</option>
                                    <option value="50">Filipenses</option>
                                    <option value="51">Colosenses</option>
                                    <option value="52">1 Tesalonicenses</option>
                                    <option value="53">2 Tesalonicenses</option>
                                    <option value="54">1 Timoteo</option>
                                    <option value="55">2 Timoteo</option>
                                    <option value="56">Tito</option>
                                    <option value="57">Filemón</option>
                                    <option value="58">Hebreos</option>
                                    <option value="59">Santiago</option>
                                    <option value="60">1 Pedro</option>
                                    <option value="61">2 Pedro</option>
                                    <option value="62">1 Juan</option>
                                    <option value="2 Juan">2 Juan</option>
                                    <option value="3 Juan">3 Juan</option>
                                    <option value="Judas">Judas</option>
                                    <option value="Apocalipsis">Apocalipsis</option>

                                </select>

                            </div>
                            <div class="form-group">
                                <label for="">Pasaje lectura</label>
                                <input type="text" class="form-control" id="" placeholder="" name="pasaje">
                            </div>

                            <div class="form-group">
                                <label for="">Turno</label>
                                <select class="form-control" name="turno">Turno
                                    <option value=""></option>
                                    <option value="Mañana">Mañana</option>
                                    <option value="Tarde">Tarde</option>
                                </select></div>
                        </div>

                        <div class="form-row archivo" style="display:none;">

                            <div class="form-group ">
                                <label for="" class="">Subir pdf</label>
                                <input type="file" class="form-control" id="" name="pdf"><br><br>

                                <label for="" class="archivo">Subir pptx</label>
                                <input type="file" class="form-control" id="" name="pptx">
                            </div>
                        </div> <br><br>


                        <div class="form-group">
                            <label for="exampleFormControlSelect1">* Expositor</label>
                            <select class="form-control" id="" name="nombre_expositor">
                                <option value="<?php echo $dato['fk_expositor'] ?>"><?php echo $dato['nombre_expositor'] ?></option>
                                <?php foreach ($expositor as $datos) : ?>
                                    <option value="<?php echo $datos[0] ?>"><?php echo $datos['nombre_expositor'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <!-- Se empleará para mostrar la foto actual y si corresponde, se verá la nuevo antes de actualizar -->
                        <div class="form-group" hidden>
                            <label for="" class="">Imagen</label>
                            <td><img src="<?php echo '../modelo/imagen/' .  $dato['imagen']; ?>" type="img" alt="avatar" accept="image/*" height="52" width="52"></td>

                        </div>
                        <br>
                        <div class="form-group">
                            <label for="" class="">Serie</label>
                            <select class="form-control" id="" name="nombre_serie">
                                <option value="<?php echo $dato['fk_serie'] ?>"><?php echo $dato['nombre_serie'] ?></option>                            
                                <?php foreach ($serie as $datoss) : ?>
                                    <option value="<?php echo $datoss[0] ?>"><?php echo $datoss['nombre_serie'] ?></option>
                                <?php endforeach; ?>
                            </select><br>

                        </div>

                        <div class="form-group" style="display:show;" id="serie2">
                            <label for="" class="">Serie 2</label>
                            <select class="form-control" id="" name="nombre_serie_2">
                                <option value="<?php echo $dato['fk_serie_2'] ?>"><?php echo $dato['nombre_serie_2'] ?></option>
                                <?php foreach ($serie_2 as $dat) : ?>
                                    <option value="<?php echo $dat[0] ?>"><?php echo $dat['nombre_serie_2'] ?></option>
                                <?php endforeach; ?>
                            </select><br>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="" class="">Comentario</label>
                            <textarea value="<?php echo $dato['comentario_audio']; ?>" class="form-control" id="" cols="15" rows="5" name="comentario_audio"><?php echo $dato['comentario_audio']; ?></textarea>

                        </div>

                        <br>
                        <br>

                        <input type="submit" class="submit-btn" style="width:100%;" value="Actualizar" name="actualizar" id="actualizar">
                        <br><br><br>

                    <?php endforeach; ?>

                </form>
            </div><br><br><br>
            <div class="col-sm-4"></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-migrate-3.3.1.js"></script>
    <script type="text/javascript" src="../modelo/js/javascript.js"></script>
    <script type="text/javascript" src="../modelo/js/alertify.js"></script>
</body>