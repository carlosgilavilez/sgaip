<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="widlabel=device-widlabel, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../modelo/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../modelo/css/alertify.css">

    <title>Nueva serie | sgaip</title>

</head>

<body>

    <div class="container-fluid">
        <h2 class="titulo">Expositor</h2>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <a href="cerrar.php">Cerrar Sesion</a><br>
                <a href="panel.php">Volver al panel</a><br>
                <ul>
                    <li>Los campso con asteriscos son obligatoris</li>
                    <li>Los audios se deben insertar en formato mp3</li>
                </ul>

            </div>
            <div class="col-sm-4">
                <form action="../modelo/expositor_m.php" id="form_insertar" onsubmit="return insertar_audio();" name="expositor_m" method="POST" enctype="multipart/form-data" class="formulario audio">
                    <div class="form-group">
                        <label for="">Expositor</label>
                        <input type="text" class="form-control" autocomplete="on" name="nombre_expositor" required>
                    </div>

                    <br>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">* Expositor/Predicador</label>
                        <select class="form-control" id="" name="">
                            <option value=""></option>
                            <option value="Aaron Hoak">Aaron Hoak</option>
                            <option value="Alfonso Ramírez">Alfonso Ramírez</option>
                            <option value="Amaro Rodríguez">Amaro Rodríguez</option>
                            <option value="Andrés Birch">Andrés Birch</option>
                            <option value="Andrew Dawson">Andrew Dawson</option>
                            <option value="Antonio Rodríguez">Antonio Rodríguez</option>
                            <option value="Antonio Torres">Antonio Torres</option>
                            <option value="Arturo Terrazas">Arturo Terrazas</option>
                            <option value="Bill &amp; Sharon James">Bill &amp; Sharon James</option>
                            <option value="Bill James">Bill James</option>
                            <option value="Carlos Gaetani">Carlos Gaetani</option>
                            <option value="Daniel Fantelli">Daniel Fantelli</option>
                            <option value="Daniel Monje">Daniel Monje</option>
                            <option value="Daniel Rodríguez">Daniel Rodríguez</option>
                            <option value="David Barceló">David Barceló</option>
                            <option value="David García">David García</option>
                            <option value="David Ide">David Ide</option>
                            <option value="David Stevens">David Stevens</option>
                            <option value="Derek Bigg">Derek Bigg</option>
                            <option value="Emilio Polo">Emilio Polo</option>
                            <option value="Fernando Carrión">Fernando Carrión</option>
                            <option value="Floyd Dauber">Floyd Dauber</option>
                            <option value="Gustavo Brunet">Gustavo Brunet</option>
                            <option value="Helena Stevens">Helena Stevens</option>
                            <option value="Ian Jemmett">Ian Jemmett</option>
                            <option value="Ignacio del Águila">Ignacio del Águila</option>
                            <option value="J. Antonio Álvarez &quot;Fossy&quot">J. Antonio Álvarez &quot;Fossy&quot</option>
                            <option value="Jaime Pizá">Jaime Pizá</option>
                            <option value="Jaime Santamaría">Jaime Santamaría</option>
                            <option value="Jaume Llenas">Jaume Llenas</option>
                            <option value="Javier Ballesteros">Javier Ballesteros</option>
                            <option value="Javier Díez">Javier Díez</option>
                            <option value="Javier Martín">Javier Martín</option>
                            <option value="Javier Pérez Patiño">Javier Pérez Patiño</option>
                            <option value="Jean Woods">Jean Woods</option>
                            <option value="Joel González">Joel González</option>
                            <option value="José de Segovia">José de Segovia</option>
                            <option value="José Luis López">José Luis López</option>
                            <option value="José María Vázquez">José María Vázquez</option>
                            <option value="José Moreno">José Moreno</option>
                            <option value="Luis Cano">Luis Cano</option>
                            <option value="Marcelo Fantelli">Marcelo Fantelli</option>
                            <option value="Martin Leech">Martin Leech</option>
                            <option value="Mateo Hill">Mateo Hill</option>
                            <option value="Nathan &amp; Brooke Santamaría">Nathan &amp; Brooke Santamaría</option>
                            <option value="Pablo Jalil">Pablo Jalil</option>
                            <option value="Patricio Ledesma">Patricio Ledesma</option>
                            <option value="Pilar Herrera">Pilar Herrera</option>
                            <option value="Rafael Manzanares">Rafael Manzanares</option>
                            <option value="Raúl de la Oz">Raúl de la Oz</option>
                            <option value="Reynaldo Rueda">Reynaldo Rueda</option>
                            <option value="Robert Memba">Robert Memba</option>
                            <option value="Rubén Sarrión">Rubén Sarrión</option>
                            <option value="Ruth Lorente">Ruth Lorente</option>
                            <option value="Sebastián Seguí">Sebastián Seguí</option>
                            <option value="Sharon James">Sharon James</option>
                            <option value="Steve Phillips">Steve Phillips</option>
                            <option value="Steve Storey">Steve Storey</option>
                            <option value="Toni Pons">Toni Pons</option>
                            <option value="Trevor Thomas">Trevor Thomas</option>
                            <option value="Víctor DeFranchi">Víctor DeFranchi</option>
                            <option value="Y. Abu Sofía">Y. Abu Sofía</option>
                            <option value="David Burt">David Burt</option>
                            <option value="Eduardo Bracier">Eduardo Bracier</option>
                            <option value="David Butler">David Butler</option>
                            <option value="Samuel García">Samuel García</option>
                            <option value="Miguel Juez">Miguel Juez</option>
                            <option value="Manuel Brambila">Manuel Brambila</option>
                            <option value="Julio R. Pérez">Julio R. Pérez</option>
                            <option value="Graciela Giménez">Graciela Giménez</option>
                            <option value="Carlos Gil">Carlos Gil</option>
                            <option value="Chris Mathieson">Chris Mathieson</option>
                            <option value="Marcos Lumbreras">Marcos Lumbreras</option>
                            <option value="Andrés Reid">Andrés Reid</option>
                            <option value="José Manuel Gil">José Manuel Gil</option>
                            <option value="Eduardo de la Piedra">Eduardo de la Piedra</option>
                            <option value="Israel Sanz">Israel Sanz</option>
                            <option value="Javier Pérez (Barcelona)">Javier Pérez (Barcelona)</option>
                            <option value="Giancarlo Montemayor">Giancarlo Montemayor</option>
                            <option value="Juan Vicente Corbí">Juan Vicente Corbí</option>
                            <option value="Samuel Sánchez">Samuel Sánchez</option>
                            <option value="Ihor Dovzhyk">Ihor Dovzhyk</option>
                            <option value="John George">John George</option>
                            <option value="Nohemí Gómez-Pimpollo">Nohemí Gómez-Pimpollo</option>
                            <option value="Pedro Blois">Pedro Blois</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="" class="">Imagen</label>
                        <input type="file" class="form-control" id="" placeholder="Subir avatar" name="imagen" accept="image/*">
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Comentario</label>
                        <textarea class="form-control" id="" cols="15" rows="5" placeholder="" name="comentario_expositor" autocomplete="on"></textarea>

                    </div>
                    <br>
                    <br>

                    <input type="submit" class="submit-btn" style="width:100%;" value="Agregar expositor" name="introducir" id="enviar">
                    <br><br><br>
                </form>
            </div><br><br><br>
            <div class="col-sm-4">
                <div class="container-fluid">

                    <div class="table-responsive-xl">
                        <table class="table table-bordered table-dark  table-hover table-sm">

                            <tr class="bg-primary">
                                <th class="bg-primary sticky">Nombre</th>
                                <th class="bg-primary sticky">Imagen</th>
                                <th class="bg-primary sticky">Comentario</th>
                            </tr>

                            <?php include_once('../modelo/expositor_m.php'); ?>
                            <?php foreach ($expositor as $dato) : ?>
                                <tr>

                                    <td><?php echo $dato['nombre_expositor'] ?></td>

                                    <td><img src="<?php echo '../modelo/imagen/' .  $dato['imagen']; ?>" type="img" alt="avatar" accept="image/*" height="58" width="62"></td>

                                    <td><?php echo $dato['comentario_expositor'] ?></td>



                                </tr>

                            <?php endforeach; ?>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-migrate-3.3.1.js"></script>
    <!-- <script type="text/javascript" src="js/ajax.js"></script> -->
    <script type="text/javascript" src="../modelo/js/javascript.js"></script>
    <script type="text/javascript" src="../modelo/js/alertify.js"></script>

</body>

</html>