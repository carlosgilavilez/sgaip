<?php include_once '../modelo/tabla_m.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

    <title>Tabla de audios | sgaip</title>

    <link rel="stylesheet" type="text/css" href="../modelo/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../modelo/css/alertify.css">

</head>

<body>


    <h2 style="text-align:center;">Expositores</h2>
    <a href="cerrar.php">Cerrar Sesion</a><br>
    <a href="panel.php">Volver al panel</a><br>
    <div class="contenedor">
        <section class="paginacion">
            <ul>
                <!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
                <?php if ($pagina == 1) : ?>
                    <li class="disabled">&laquo;</li>
                <?php else : ?>
                    <li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
                <?php endif; ?>

                <!-- Ejecutamos un ciclo para mostrar las paginas, las cuales se crean según el num del $i -->
                <?php
                for ($i = 1; $i <= $numeroPaginas; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
                    } else {
                        echo "<li><a href='?pagina=$i'>$i</a></li>";
                    }
                }
                ?>
                <!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
                <?php if ($pagina == $numeroPaginas) : ?>
                    <li class="disabled">&raquo;</li>
                <?php else : ?>
                    <li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
                <?php endif; ?>
            </ul>
        </section>
    </div>

    <div class="container-fluid">

        <div class="table-responsive-xl">
            <table class="table table-bordered table-dark  table-hover table-sm">

                <tr class="bg-primary">
                    <th class="bg-primary sticky">Nombre del audio</th>
                    <th class="bg-primary sticky">Fecha</th>
                    <th class="bg-primary sticky">Audio</th>
                    <th class="bg-primary sticky">Expositor</th>
                    <th class="bg-primary sticky">Imagen</th>
                    <th class="bg-primary sticky">Categoría</th>
                    <th class="bg-primary sticky">Pdf</th>
                    <th class="bg-primary sticky">Pptx</th>
                    <th class="bg-primary sticky">Serie</th>
                    <th class="bg-primary sticky">Serie 2</th>
                    <th class="bg-primary sticky">Texto biblíco</th>
                    <th class="bg-primary sticky">Turno</th>
                    <th class="bg-primary sticky">Comentario</th>
                    <th class="bg-primary sticky">Acción</th>

                </tr>

                <?php  foreach ($audios as $dato) : ?>
                    <tr>
                        <td><?php echo $dato['nombre_audio'] ?></td>

                        <td><?php echo $dato['fecha_audio'] ?></td>

                        <td style="width:15px;"><audio controls> <source src="<?php echo '../modelo/audio/' .  $dato['audio']; ?>" type="audio/mpeg"  ></source>
                      </audio controls preload="none"></td> 

                        <td><?php echo $dato['nombre_expositor'] ?></td>

                        <td><img src="<?php echo '../modelo/imagen/' .  $dato['imagen']; ?>" type="img" alt="avatar" accept="image/*" height="52" width="52"></td>

                        <td><?php echo $dato['categoria'] ?></td>

                        <td><a href="../modelo/pdf/<?php echo $dato['pdf']; ?>" target="_blank"><?php echo $dato['pdf']; ?></a></td>

                        <td><a href="../modelo/pptx/<?php echo $dato['pptx']; ?>"><?php echo $dato['pptx']; ?></a></td>

                        <td><?php echo $dato['nombre_serie'] ?></td>

                        <td><?php echo $dato['nombre_serie_2'] ?></td>

                        <td><?php echo $dato['libro'] . " " . $dato['pasaje'] ?></td>

                        <td><?php echo $dato['turno'] ?></td>

                        <td><?php echo $dato['comentario_audio'] ?></td>

                        <td><a href="modificar.php?id=<?php echo $dato['id_audio']; ?>">Editar</a></td>
                        <td> <a href="eliminar.php?id=<?php echo $dato['id_audio']; ?>">Eliminar</a></td>

                    </tr>

                <?php endforeach; ?>

            </table>
        </div>

    </div>


    <div class="contenedor">
        <section class="paginacion">
            <ul>
                <!-- Establecemos cuando el boton de "Anterior" estara desabilitado -->
                <?php if ($pagina == 1) : ?>
                    <li class="disabled">&laquo;</li>
                <?php else : ?>
                    <li><a href="?pagina=<?php echo $pagina - 1 ?>">&laquo;</a></li>
                <?php endif; ?>

                <!-- Ejecutamos un ciclo para mostrar las paginas, las cuales se crean según el num del $i -->
                <?php
                for ($i = 1; $i <= $numeroPaginas; $i++) {
                    if ($pagina == $i) {
                        echo "<li class='active'><a href='?pagina=$i'>$i</a></li>";
                    } else {
                        echo "<li><a href='?pagina=$i'>$i</a></li>";
                    }
                }
                ?>
                <!-- Establecemos cuando el boton de "Siguiente" estara desabilitado -->
                <?php if ($pagina == $numeroPaginas) : ?>
                    <li class="disabled">&raquo;</li>
                <?php else : ?>
                    <li><a href="?pagina=<?php echo $pagina + 1 ?>">&raquo;</a></li>
                <?php endif; ?>
            </ul>
        </section>
    </div>
</body>
</html>

