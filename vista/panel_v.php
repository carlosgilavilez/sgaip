<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="widlabel=device-widlabel, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../modelo/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../modelo/css/alertify.css">

    <title>Panel de control | sgaip</title>

</head>

<body>

    <div class="container-fluid">
        <br>
        <h2 class="titulo">Panel de control</h2>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                <a href="cerrar.php">Cerrar Sesion</a><br>


                <ul>
                    <li>Solo se podra agregar el nuevo audio si existe el expositor</li>
                    <li>Si el audio pertenece a una serie, deberás crear primero la serie en caso que aún no este creada</li>
                    <li>Los audios se deben insertar en formato mp3</li>
                </ul>

            </div>

            <div class="col-sm-8">

                <a href="nuevo_audio.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Agregar nuevo audio</a></span>

                <a href="tabla.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Ver tabla de audios</a>

                <a href="serie.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Módulo de series</a>

                <a href="expositor.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Módulo de expositores</a>

                <a href="nuevo_usuario.php" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Crear nuevo usuario</a>

            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-migrate-3.3.1.js"></script>
    <!-- <script type="text/javascript" src="js/ajax.js"></script> -->
    <script type="text/javascript" src="../modelo/js/javascript.js"></script>
    <script type="text/javascript" src="../modelo/js/alertify.js"></script>

</body>

</html>