<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>


    <meta charset="UTF-8">
    <meta name="viewport" content="widlabel=device-widlabel, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../modelo/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../modelo/css/alertify.css">

    <title>Nuevo audio | sgaip</title>

</head>

<body>

    <div class="container-fluid">
        <h2 class="titulo">Inserción de nuevo audio</h2>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <a href="cerrar.php">Cerrar Sesion</a><br>
                <a href="panel.php">Volver al panel</a><br>
                <ul>
                    <li>Los campso con asteriscos son obligatoris</li>
                    <li>Los audios se deben insertar en formato mp3</li>
                </ul>

            </div>
            <div class="col-sm-4">
                <form action="../modelo/nuevo_audio_m.php" id="form_insertar" onsubmit="return insertar_audio()" name="nuevo_audio_m" method="POST" enctype="multipart/form-data" class="formulario audio">
                    <div class="form-group">
                        <label for="">* Nombre</label>
                        <input type="text" class="form-control" autocomplete="on" name="nombre_audio" required>
                    </div>

                    <div class="form-group ">
                        <label for="">Audio</label>
                        <input type="file" class="form-control" id="audio_mp3" placeholder="Subir el audio" name="audio" required>
                    </div>
                    <br>

                    <div class="form-group ">
                        <label for="">Fecha</label>
                        <input type="date" class="form-control" name="fecha_audio">
                    </div>
                    <br>

                    <div class="form-group">
                        <label for="">* Categoría </label>
                        <select class="form-control" id="categoria" name="categoria" required>
                            <option value="0"></option>
                            <option value="predicacion">Predicación</option>
                            <option value="tema esencial">Tema esencial</option>
                            <option value="otro">Otro</option>

                        </select>
                    </div>

                    <div class="form-row cita" style="display:none;" id="cita_bloque">
                        <div class="form-group" id="cita">
                            <label for="">Libro de la Biblia</label>
                            <select class="form-control" name="libro">
                                <option value=""></option>
                                <option value="Génesis">Génesis</option>
                                <option value="Éxodo">Éxodo</option>
                                <option value="Levítico">Levítico</option>
                                <option value="Números">Números</option>
                                <option value="Deuteronomio">Deuteronomio</option>
                                <option value="Josué">Josué</option>
                                <option value="Jueces">Jueces</option>
                                <option value="Rut">Rut</option>
                                <option value="1 Samuel">1 Samuel</option>
                                <option value="2 Samuel">2 Samuel</option>
                                <option value="1 Reyes">1 Reyes</option>
                                <option value="2 Reyes">2 Reyes</option>
                                <option value="1 Crónicas">1 Crónicas</option>
                                <option value="2 Crónicas">2 Crónicas</option>
                                <option value="Esdras">Esdras</option>
                                <option value="Nehemías">Nehemías</option>
                                <option value="Ester">Ester</option>
                                <option value="Job">Job</option>
                                <option value="Salmos">Salmos</option>
                                <option value="Proverbios">Proverbios</option>
                                <option value="Eclesiastés">Eclesiastés</option>
                                <option value="El Cantar de los Cantares">El Cantar de los Cantares</option>
                                <option value="Isaías">Isaías</option>
                                <option value="Jeremías">Jeremías</option>
                                <option value="Lamentaciones">Lamentaciones</option>
                                <option value="Ezequiel">Ezequiel</option>
                                <option value="Daniel">Daniel</option>
                                <option value="Oseas">Oseas</option>
                                <option value="Joel">Joel</option>
                                <option value="Amós">Amós</option>
                                <option value="Abdías">Abdías</option>
                                <option value="Jonás">Jonás</option>
                                <option value="Miqueas">Miqueas</option>
                                <option value="Nahúm">Nahúm</option>
                                <option value="Habacuc">Habacuc</option>
                                <option value="Sofonías">Sofonías</option>
                                <option value="Zacarías">Ageo</option>
                                <option value="38">Zacarías</option>
                                <option value="Malaquías">Malaquías</option>
                                <option value=""></option>
                                <option value="Mateo">Mateo</option>
                                <option value="Marcos">Marcos</option>
                                <option value="Lucas">Lucas</option>
                                <option value="Juan">Juan</option>
                                <option value="Hechos">Hechos</option>
                                <option value="Romanos">Romanos</option>
                                <option value="1 Corintios">1 Corintios</option>
                                <option value="2 Corintios">2 Corintios</option>
                                <option value="Gálatas">Gálatas</option>
                                <option value="Efesios">Efesios</option>
                                <option value="Filipenses">Filipenses</option>
                                <option value="Colosenses">Colosenses</option>
                                <option value="1 Tesalonicenses">1 Tesalonicenses</option>
                                <option value="2 Tesalonicenses">2 Tesalonicenses</option>
                                <option value="1 Timoteo">1 Timoteo</option>
                                <option value="2 Timoteo">2 Timoteo</option>
                                <option value="Tito">Tito</option>
                                <option value="Filemón">Filemón</option>
                                <option value="Hebreos">Hebreos</option>
                                <option value="Santiago">Santiago</option>
                                <option value="1 Pedro">1 Pedro</option>
                                <option value="2 Pedro">2 Pedro</option>
                                <option value="1 Juan">1 Juan</option>
                                <option value="2 Juan">2 Juan</option>
                                <option value="3 Juan">3 Juan</option>
                                <option value="Judas">Judas</option>
                                <option value="Apocalipsis">Apocalipsis</option>
                            </select>

                        </div>
                        <div class="form-group ">
                            <label for="">Pasaje</label>
                            <input type="text" class="form-control" id="" placeholder="" name="pasaje">
                        </div>

                        <div class="form-group ">
                            <label for="">Turno</label>
                            <select class="form-control" name="turno">
                                <option value=""></option>
                                <option value="Mañana">Mañana</option>
                                <option value="Tarde">Tarde</option>
                            </select></div>
                    </div>

                    <div class="form-row archivo" style="display:none;">

                        <div class="form-group ">
                            <label for="" class="">Subir pdf</label>
                            <input type="file" class="form-control" id="" name="pdf"><br><br>

                            <label for="" class="archivo">Subir pptx</label>
                            <input type="file" class="form-control" id="" name="pptx">
                        </div>
                    </div> <br>

                    <div class="form-group">
                        <label for="exampleFormControlSelect1">* Expositor</label>
                        <select class="form-control" id="" name="nombre_expositor" required>
                            <!--Muestra todas los expositores de la base de datos -->
                            <option value=""></option>
                            <?php require_once '../modelo/nuevo_audio_m.php'; ?>
                            <?php foreach ($expositor as $datos) : ?>
                                <option value="<?php echo $datos[0] ?>"><?php echo $datos['nombre_expositor'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div> <br>

                    <div class="form-group">
                        <label for="exampleFormControlSelect1" class="">Serie</label>
                        <select id="" class="form-control" name="nombre_serie">
                            <option value="4"></option>
                            <!--Muestra todas las series de la base de datos -->
                            <?php foreach ($serie as $dato) : ?>
                                <option value="<?php echo $dato[0] ?>"><?php echo $dato['nombre_serie'] ?></option>
                            <?php endforeach; ?>
                        </select><br>

                        <label for="" class="">¿Pertenece a otra serie? SI: </label> <input type="checkbox" name="" id="serie_2">
                    </div>

                    <div class="form-group" style="display:none;" id="serie2">
                        <label for="" class="">Serie 2</label>
                        <select id="" class="form-control" name="nombre_serie_2">
                            <option value="4"></option>
                            <!--Muestra todas las series de la base de datos -->
                            <?php foreach ($serie_2 as $dat) : ?>
                                <option value="<?php echo $dat[0] ?>"><?php echo $dat['nombre_serie_2'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <br>
                    <div class="form-group">
                        <label for="" class="">Comentario</label>
                        <textarea class="form-control" id="" cols="20" rows="6" placeholder="" name="comentario_audio" autocomplete="on"></textarea>

                    </div>
                    <br>
                    <br>

                    <input type="submit" class="submit-btn" style="width:100%;" value="Subir audio" name="insertar" id="enviar">
                    <br><br><br>
                </form>
            </div><br><br><br>
            <div class="col-sm-4"></div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-migrate-3.3.1.js"></script>
    <!-- <script type="text/javascript" src="js/ajax.js"></script> -->
    <script type="text/javascript" src="../modelo/js/javascript.js"></script>
    <script type="text/javascript" src="../modelo/js/alertify.js"></script>

</body>

</html>